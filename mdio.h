#ifndef NIXIE_MDIO_H_
#define NIXIE_MDIO_H_

#include <stdint.h>

/**
 * Initializes MDIO hardware.
 */
void mdio_init(void);

 /**
  * Reads the specified PHY register.
  *
  * @param reg  The register to read.
  */
uint16_t mdio_read(uint8_t reg);

/**
 * Writes to the specified PHY register.
 *
 * @param reg    The register to write to.
 * @param value  The value to write.
 */ 
void mdio_write(uint8_t reg, uint16_t value);

#endif /* NIXIE_MDIO_H_ */