#include "syscall.h"

#include <sys/stat.h>
#include <sys/times.h>
#include <sys/time.h>
#include <sys/types.h>

#include <FreeRTOS.h>
#include <semphr.h>

#include "serial.h"
#include "clock.h"

#include <errno.h>
#undef errno
extern int errno;

static xSemaphoreHandle envMutex;

static char *defaultEnv[] = { 0 };
char **environ = defaultEnv;

int _close(int file)
{
	return -1;
}

int _execve(char *name, char **argv, char **env)
{
	errno = ENOMEM;
	return -1;
}

int _fork(void)
{
	errno = EAGAIN;
	return -1;
}

int _fstat(int file, struct stat *st)
{
	st->st_mode = S_IFCHR;
	return 0;
}

int _getpid(void)
{
	return 1;
}

int _isatty(int file)
{
	return 1;
}

int _kill(int pid, int sig)
{
	errno = EINVAL;
	return -1;
}

int _link(char *old, char *new)
{
	errno = EMLINK;
	return -1;
}

int _lseek(int file, int ptr, int dir)
{
	return 0;
}

int _open(const char *name, int flags, int mode)
{
	return -1;
}

int _read(int file, char *ptr, int len)
{
	return 0;
}

int _stat(char *file, struct stat *st)
{
	st->st_mode = S_IFCHR;
	return 0;
}

int _times(struct tms *buf)
{
	return -1;
}

int _unlink(char *name)
{
	errno = ENOENT;
	return -1;
}

int _wait(int *status)
{
	errno = ECHILD;
	return -1;
}

void _exit(int status)
{
	for(;;);
}

int _write(int file, char *ptr, int len)
{
	serial_printLines(ptr, len);
	return len;
}

int _gettimeofday(struct timeval *tv, struct timezone *tz)
{
	uint64_t t = clock_currentTime();
	tv->tv_sec = t / 1000;
	tv->tv_usec = (t % 1000) * 1000;
	return 0;
}

void __env_lock (struct _reent *reent)
{
	xSemaphoreTakeRecursive(envMutex, portMAX_DELAY);
}

void __env_unlock (struct _reent *reent)
{
	xSemaphoreGiveRecursive(envMutex);
}

void syscall_init(void)
{
	envMutex = xSemaphoreCreateRecursiveMutex();
}
