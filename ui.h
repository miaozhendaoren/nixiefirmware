#ifndef NIXIE_UI_H_
#define NIXIE_UI_H_

#include <stdint.h>

/**
 * Initializes the user interface.
 */
 void ui_init(void);

/**
 * Sets the display mode.
 */
void ui_toggleDisplayMode(void);

/**
 * Toggles the display sub mode.
 */
void ui_toggleDisplaySubMode(void);

/**
 * Toggles the display on/off.
 */
void ui_toggleEnable(void);

/**
 * Toggles the backlight on/off.
 */
void ui_toggleEnableBacklight(void);

/**
 * Makes the UI task reread the timezone string from the
 * environment.
 */
void ui_updateTimezone(void);

#endif /* NIXIE_UI_H_ */