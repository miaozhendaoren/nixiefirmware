#include "serial.h"

#include <lpc17xx.h>
#include <lpc17xx_pinsel.h>
#include <lpc17xx_uart.h>
#include <lpc17xx_clkpwr.h>

void serial_init(void)
{
	PINSEL_CFG_Type pinCfg;

	pinCfg.Portnum = 2;
	pinCfg.Funcnum = 2;
	pinCfg.OpenDrain = PINSEL_PINMODE_NORMAL;

	pinCfg.Pinnum = 0;
	PINSEL_ConfigPin(&pinCfg);
	pinCfg.Pinnum = 1;
	PINSEL_ConfigPin(&pinCfg);

	UART_CFG_Type uartCfg;
	UART_ConfigStructInit(&uartCfg);
	UART_Init(LPC_UART1, &uartCfg);

	UART_FIFO_CFG_Type fifoCfg;
	UART_FIFOConfigStructInit(&fifoCfg);
	UART_FIFOConfig(LPC_UART1, &fifoCfg);

	UART_TxCmd(LPC_UART1, ENABLE);
}

void serial_write(char *data, int len)
{
	UART_Send(LPC_UART1, data, len, BLOCKING);
}

void serial_printLines(char *data, int len)
{
	char cr = '\r';
	for (int i = 0; i < len; i++) {
		if (data[i] == '\n')
			UART_Send(LPC_UART1, &cr, 1, BLOCKING);

		UART_Send(LPC_UART1, data + i, 1, BLOCKING);
	}
}