#ifndef NIXIE_SYSCALL_H_
#define NIXIE_SYSCALL_H_

/**
 * Initializes everything syscall related. Must be called
 * before newlib methods are used.
 */
void syscall_init(void);

#endif /* NIXIE_SYSCALL_H_ */