#ifndef NIXIE_SNTP_H_
#define NIXIE_SNTP_H_

///The maximum hostname length.
#define SNTP_MAX_HOSTNAME_LEN 63

/**
 * Initializes and start the SNTP client service.
 */
void sntp_init(void);

/**
 * Sets the server to sync from.
 *
 * @param server  The server hostname.
 */
void sntp_setServer(const char *server);

/**
 * Writes the hostname of the server that is synced from
 * to the specified buffer. The buffer must be at least
 * SNTP_MAX_HOSTNAME_LEN in size.
 *
 * @return dest  The server hostname.
 */
void sntp_getServer(char *dest);

/**
 * Sets the sync interval.
 *
 * @param inteval  The sync interval in seconds.
 */
void sntp_setSyncInterval(int interval);

/**
 * Returns the sync interval.
 *
 * @return The sync interval in seconds.
 */
int sntp_getSyncInterval();

#endif /* NIXIE_SNTP_H_ */