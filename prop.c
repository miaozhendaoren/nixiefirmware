#include "prop.h"

#include <stdlib.h>
#include <string.h>
#include <stdio.h>

#include "iap.h"
#include "sntp.h"
#include "ui.h"

#define PROP_SECTOR_NUMBER 29
#define PROP_SECTOR_BASE ((const uint8_t *)(0x00078000))

static char nodeName[NODE_NAME_MAX_LEN + 1] = "nixie";

static char *persistentProps[] = {
	"name", "ntpServer", "ntpSyncInterval", "tzString", NULL
};

int prop_get(const char *name, char *buf, size_t len)
{
	if (strcmp(name, "name") == 0) {
		return sniprintf(buf, len, "%s", nodeName);
	} else if (strcmp(name, "ntpServer") == 0) {
		char serverHostname[SNTP_MAX_HOSTNAME_LEN];
		sntp_getServer(serverHostname);
		return sniprintf(buf, len, "%s", serverHostname);
	} else if (strcmp(name, "ntpSyncInterval") == 0) {
		return sniprintf(buf, len, "%d", sntp_getSyncInterval());
	} else if (strcmp(name, "tzString") == 0) {
		char *value = getenv("TZ");
		if (value == NULL) {
			buf[0] = 0;
			return 0;
		} else {
			return sniprintf(buf, len, "%s", value);
		}
	}

	return 0;
}

int prop_set(const char *name, const char *value)
{
	if (strcmp(name, "name") == 0) {
		sniprintf(nodeName, sizeof(nodeName), "%s", value);
		return 1;
	} else if (strcmp(name, "ntpServer") == 0) {
		sntp_setServer(value);
		return 1;
	} else if (strcmp(name, "ntpSyncInterval") == 0) {
		sntp_setSyncInterval(atoi(value));
		return 1;
	} else if (strcmp(name, "tzString") == 0) {
		setenv("TZ", value, 1);
		ui_updateTimezone();
		return 1;
	}

	return 0;
}

void prop_savePersistent(void)
{
	char __attribute__((aligned(4))) value[PROP_VALUE_MAX_LEN + 1];
	for (int i = 0; persistentProps[i] != NULL; i++) {
		prop_get(persistentProps[i], value, sizeof(value));
		iap_prepareSectors(PROP_SECTOR_NUMBER, PROP_SECTOR_NUMBER);
		iap_copyRamToFlash(PROP_SECTOR_BASE + (sizeof(value) * i), (const uint8_t *)value, sizeof(value));
	}
}

void prop_loadPersistent(void)
{
	//0xFF is not a valid ASCII character and is what will be set
	//if the persistence sector is freshly erased and does not contain
	//valid data.
	if (PROP_SECTOR_BASE[0] == 0xFF)
		return;

	for (int i = 0; persistentProps[i] != NULL; i++)
		prop_set(persistentProps[i], PROP_SECTOR_BASE + ((PROP_VALUE_MAX_LEN + 1) * i));
}