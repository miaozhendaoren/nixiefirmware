#ifndef NIXIE_IR_H_
#define NIXIE_IR_H_

/**
 * Initializes the IR module.
 */
 void ir_init(void);

/**
 * IR timer interrupt handler.
 */
void ir_interrupt(void);

#endif /* NIXIE_IR_H_ */