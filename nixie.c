#include "nixie.h"

#include <lpc17xx_ssp.h>
#include <lpc17xx_pinsel.h>
#include <lpc17xx_gpio.h>
#include <lpc17xx_pwm.h>

#define RCLK_PORT	1
#define RCLK_PIN	22
#define RCLK_GPIO	LPC_GPIO1

#define DIGIT_LDP		0
#define DIGIT_RDP		10
#define DIGIT_SPACE		15

#define FADE_STAGES 	8
#define FADE_TIME_SCALE	50.0f

uint8_t digitMap[] = { 12, 4, 8, 11, 3, 13, 5, 9, 1, 2 };

void nixie_init(void)
{
	//Pin muxing
	PINSEL_CFG_Type pinCfg;
	pinCfg.OpenDrain = PINSEL_PINMODE_NORMAL;
	pinCfg.Portnum = 1;

	//MOSI/SDI
	pinCfg.Funcnum = 3;
	pinCfg.Pinnum = 24;
	PINSEL_ConfigPin(&pinCfg);

	//SCLK
	pinCfg.Funcnum = 3;
	pinCfg.Pinnum = 20;
	PINSEL_ConfigPin(&pinCfg);

	//LED PWM
	pinCfg.Pinnum = 26;
	pinCfg.Funcnum = 2;
	PINSEL_ConfigPin(&pinCfg);

	//RCLK
	pinCfg.Portnum = RCLK_PORT;
	pinCfg.Pinnum = RCLK_PIN;
	pinCfg.Funcnum = 0;
	PINSEL_ConfigPin(&pinCfg);
	FIO_SetDir(RCLK_PORT, 1 << RCLK_PIN, 1);
	FIO_ClearValue(RCLK_PORT, 1 << RCLK_PIN);

	//SSP
	SSP_CFG_Type sspCfg;
	sspCfg.ClockRate = 80000;
	sspCfg.FrameFormat = SSP_FRAME_SPI;
	sspCfg.Databit = SSP_DATABIT_16;
	sspCfg.Mode = SSP_MASTER_MODE;
	sspCfg.CPOL = 0;
	sspCfg.CPHA = 0;
	SSP_Init(LPC_SSP0, &sspCfg);
	SSP_Cmd(LPC_SSP0, ENABLE);

	//PWM
	PWM_TIMERCFG_Type timerCfg;
	timerCfg.PrescaleOption = PWM_TIMER_PRESCALE_TICKVAL;
	timerCfg.PrescaleValue = 100;
	PWM_Init(LPC_PWM1, PWM_MODE_TIMER, &timerCfg);
	
	PWM_MatchUpdate(LPC_PWM1, 0, 256, PWM_MATCH_UPDATE_NOW);
	PWM_MATCHCFG_Type matchCfg;
	matchCfg.MatchChannel = 0;
	matchCfg.IntOnMatch = DISABLE;
	matchCfg.StopOnMatch = DISABLE;
	matchCfg.ResetOnMatch = ENABLE;
	PWM_ConfigMatch(LPC_PWM1, &matchCfg);

	//EN PWM
	matchCfg.MatchChannel = 4;
	matchCfg.IntOnMatch = DISABLE;
	matchCfg.StopOnMatch = DISABLE;
	matchCfg.ResetOnMatch = DISABLE;
	PWM_ConfigMatch(LPC_PWM1, &matchCfg);
	PWM_ChannelConfig(LPC_PWM1, 4, PWM_CHANNEL_SINGLE_EDGE);
	PWM_ChannelCmd(LPC_PWM1, 4, ENABLE);

	//LED PWM
	matchCfg.MatchChannel = 6;
	matchCfg.IntOnMatch = DISABLE;
	matchCfg.StopOnMatch = DISABLE;
	matchCfg.ResetOnMatch = DISABLE;
	PWM_ConfigMatch(LPC_PWM1, &matchCfg);
	PWM_ChannelConfig(LPC_PWM1, 6, PWM_CHANNEL_SINGLE_EDGE);
	PWM_ChannelCmd(LPC_PWM1, 6, ENABLE);

	PWM_ResetCounter(LPC_PWM1);
	PWM_CounterCmd(LPC_PWM1, ENABLE);
	PWM_Cmd(LPC_PWM1, ENABLE);

	nixie_setBrightness(0);
}

static void nixie_output(uint32_t data)
{
	while((LPC_SSP0->SR & SSP_SR_TNF) == 0);
	LPC_SSP0->DR = data & 0xFFFF;

	while((LPC_SSP0->SR & SSP_SR_TNF) == 0);
	LPC_SSP0->DR = (data >> 16) & 0xFFFF;

	while((LPC_SSP0->SR & SSP_SR_TNF) == 0);

	RCLK_GPIO->FIOSET = (1 << RCLK_PIN);
	for (volatile int i = 0; i < 10; i++);
	RCLK_GPIO->FIOCLR = (1 << RCLK_PIN);
}

static uint8_t nixie_charToBits(char c)
{
	switch (c) {
		default:
		case ' ':
			return DIGIT_SPACE;
		case ',':
			return DIGIT_LDP;
		case '.':
			return DIGIT_RDP;
		case '0':
		case '1':
		case '2':
		case '3':
		case '4':
		case '5':
		case '6':
		case '7':
		case '8':
		case '9':
			return digitMap[c - '0'];
	}
}

static uint32_t nixie_stringToBits(char *digits)
{
	uint32_t bits = 0;
	for (int i = 0; i < 8; i++)
		bits |= (nixie_charToBits(digits[i]) << (4 * (7 - i)));

	return
		((bits & 0x000000FF) << 8) |
	    ((bits & 0x0000FF00) >> 8) |
	    ((bits & 0x00FF0000) << 8) |
	    ((bits & 0xFF000000) >> 8);
}

void nixie_setDisplay(char *digits)
{
	nixie_output(nixie_stringToBits(digits));
}

void nixie_fade(char *from, char *to, float time)
{
	int fadeRepeat = (time * FADE_TIME_SCALE);
	uint32_t fromBits = nixie_stringToBits(from);
	uint32_t toBits = nixie_stringToBits(to);
	uint32_t sequence[FADE_STAGES];
	for (int i = 0; i < FADE_STAGES; i++)
		sequence[i] = fromBits;

	for (int stage = 0; stage < (FADE_STAGES - 1); stage++) {
		sequence[stage] = toBits;
		for (int repeat = 0; repeat < fadeRepeat; repeat++) {
			for(int i = 0; i < FADE_STAGES; i++)
				nixie_output(sequence[i]);
		}
	}

	nixie_output(toBits);
}

void nixie_setBrightness(uint8_t value)
{
	PINSEL_CFG_Type pinCfg;
	pinCfg.OpenDrain = PINSEL_PINMODE_NORMAL;
	pinCfg.Portnum = 1;
	pinCfg.Pinnum = 23;

	if (value > 0) {
		pinCfg.Funcnum = 2;
		PWM_MatchUpdate(
			LPC_PWM1, 4,
			255 - value,
			PWM_MATCH_UPDATE_NOW);
	} else {
		pinCfg.Funcnum = 0;
		FIO_ClearValue(1, (1 << 23));
	}

	PINSEL_ConfigPin(&pinCfg);
}

void nixie_setBacklight(uint8_t value)
{
	PWM_MatchUpdate(
		LPC_PWM1, 6,
		value,
		PWM_MATCH_UPDATE_NOW);
}