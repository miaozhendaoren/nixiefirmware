#ifndef NIXIE_PROP_H_
#define NIXIE_PROP_H_

#include <stddef.h>

/// Maximum length for the name property.
#define NODE_NAME_MAX_LEN 63
///Maximum length for a property value.
#define PROP_VALUE_MAX_LEN 255

/**
 * Writes the value of the specified property to the specified buffer. The
 * string is null terminated.
 *
 * @param name  The name of the property.
 * @param buf   The buffer to write to.
 * @param len   The buffer length.
 *
 * @return The length of the value excluding null terminator.
 */
int prop_get(const char *name, char *buf, size_t len);

/**
 * Sets the specified property to the specified value.
 *
 * @param name   The name of the property to set.
 * @param value  The value to set.
 *
 * @return \c 1 on successful setting, \c 0 if setting failed
 */
int prop_set(const char *name, const char *value);

/**
 * Saves persistent properties to flash memory.
 */
void prop_savePersistent(void);

/**
 * Loads persistent properties from flash memory.
 */
void prop_loadPersistent(void);

#endif /* NIXIE_PROP_H_ */