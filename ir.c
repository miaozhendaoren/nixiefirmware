#include "ir.h"

#include <FreeRTOS.h>
#include <task.h>
#include <queue.h>

#include <lpc17xx_timer.h>
#include <lpc17xx_pinsel.h>

#include "ui.h"

#define IR_ADDRESS		0x23
#define MAX_MSG_LENGTH	68

#define IN_RANGE(l, x, h)	(((x) >= (l)) && ((x) <= (h)))
#define IS_START(x)			IN_RANGE(8500, x, 9500)
#define IS_START_SPACE(x)	IN_RANGE(4000, x, 5000)
#define IS_REPEAT_SPACE(x)	IN_RANGE(1700, x, 2800)
#define IS_BIT_MARKER(x)	IN_RANGE(300, x, 700)
#define IS_ONE(x)			IN_RANGE(1000, x, 1800)
#define IS_ZERO(x)			IN_RANGE(300, x, 700)

#define BUTTON_POWER	12
#define BUTTON_LEFT		16
#define BUTTON_RIGHT	8

typedef struct {
	int size;
	uint16_t data[MAX_MSG_LENGTH];
} IRRawMessage;

typedef struct {
	uint8_t address;
	uint8_t command;
	int repeat;
} IRMessage;

static IRRawMessage recvMsg;
static xQueueHandle messageQueue;
static IRMessage lastMessage;
static int repeatCount = 0;

void ir_interrupt(void)
{
	signed portBASE_TYPE irTaskWoken = pdFALSE;

	if (TIM_GetIntStatus(LPC_TIM1, TIM_MR0_INT) == RESET) {
		TIM_ClearIntCapturePending(LPC_TIM1, TIM_MR0_INT);
		LPC_TIM1->TCR |= TIM_RESET;
		LPC_TIM1->TCR &= ~TIM_RESET;

		if (recvMsg.size < MAX_MSG_LENGTH)
			recvMsg.data[recvMsg.size++] = (uint16_t)TIM_GetCaptureValue(LPC_TIM1, TIM_COUNTER_INCAP0);
	} else {
		LPC_TIM1->TCR |= TIM_RESET;
		TIM_ClearIntPending(LPC_TIM1, TIM_MR0_INT);

		if (recvMsg.size >= 4)
			xQueueSendToBackFromISR(messageQueue, &recvMsg, &irTaskWoken);
		recvMsg.size = 0;
	}

	portEND_SWITCHING_ISR(irTaskWoken);
}

static int ir_decodeMessage(IRRawMessage *msg, IRMessage *decoded)
{
	//Filter out messages that are of incorrect lengths
	if ((msg->size != 4) && (msg->size != 68))
		return 0;

	if (IS_START(msg->data[1])) {
		if ((msg->size == 68) && IS_START_SPACE(msg->data[2])) {

			uint16_t *bitData = msg->data + 3;
			uint32_t bits = 0;
			for (int i = 0; i < 32; i++) {
				if (!IS_BIT_MARKER(bitData[i * 2]))
					return 0;

				if (IS_ONE(bitData[(i * 2) + 1]))
					bits |= (1 << i);
				else if (!IS_ZERO(bitData[(i * 2) + 1]))
					return 0;
			}

			uint8_t address = bits & 0xFF;
			uint8_t addressCheck = ~(bits >> 8) & 0xFF;
			uint8_t command = (bits >> 16) & 0xFF;
			uint8_t commandCheck = ~(bits >> 24) & 0xFF;
			if ((address == addressCheck) && (command == commandCheck)) {
				lastMessage.address = address;
				lastMessage.command = command;
				lastMessage.repeat = 0;
				*decoded = lastMessage;
				return 1;
			}

		} else if ((msg->size == 4) && IS_REPEAT_SPACE(msg->data[2])) {
		
			if (IS_BIT_MARKER(msg->data[3])) {
				*decoded = lastMessage;
				decoded->repeat = 1;
				return 1;
			}

		}
	}

	return 0;
}

static void ir_dispatchMessage(IRMessage *msg)
{
	//iprintf("addr = %d, cmd = %d, rep = %d\n", msg->address, msg->command, msg->repeat);
	if (msg->repeat)
		repeatCount++;
	else
		repeatCount = 0;

	switch (msg->command) {
	case BUTTON_POWER:
		if (msg->repeat) {
			if (repeatCount == 10) {
				ui_toggleEnable();
				//Toggle backlight, previous toggle on no-repeat should undone
				ui_toggleEnableBacklight();
			}
		} else {
			ui_toggleEnableBacklight();
		}
		break;

	case BUTTON_LEFT:
		if (!msg->repeat)
			ui_toggleDisplayMode();
		break;

	case BUTTON_RIGHT:
		if (!msg->repeat)
			ui_toggleDisplaySubMode();
		break;
	}
}

static void ir_task(void *param)
{
	for (;;) {
		IRRawMessage msg;
		if (xQueueReceive(messageQueue, &msg, portMAX_DELAY) == pdTRUE) {
			IRMessage decoded;
			if (ir_decodeMessage(&msg, &decoded))
				ir_dispatchMessage(&decoded);
		}
	}
}

void ir_init(void)
{
	//Pin muxing
	PINSEL_CFG_Type pinCfg;
	pinCfg.OpenDrain = PINSEL_PINMODE_NORMAL;
	pinCfg.Portnum = 1;
	pinCfg.Pinnum = 18;
	pinCfg.Funcnum = 3;
	PINSEL_ConfigPin(&pinCfg);

	//Timer config
	TIM_TIMERCFG_Type timerCfg;
	timerCfg.PrescaleOption = TIM_PRESCALE_USVAL;
	timerCfg.PrescaleValue = 1;
	TIM_Init(LPC_TIM1, TIM_TIMER_MODE, &timerCfg);

	//Capture config
	TIM_CAPTURECFG_Type captureCfg;
	captureCfg.CaptureChannel = 0;
	captureCfg.RisingEdge = ENABLE;
	captureCfg.FallingEdge = ENABLE;
	captureCfg.IntOnCaption = ENABLE;
	TIM_ConfigCapture(LPC_TIM1, &captureCfg);

	//Use match as a timeout
	TIM_MATCHCFG_Type matchCfg;
	matchCfg.MatchChannel = 0;
	matchCfg.IntOnMatch = ENABLE;
	matchCfg.StopOnMatch = DISABLE;
	matchCfg.ResetOnMatch = DISABLE;
	matchCfg.ExtMatchOutputType = TIM_EXTMATCH_NOTHING;
	matchCfg.MatchValue = 10000;
	TIM_ConfigMatch(LPC_TIM1, &matchCfg);

	NVIC_SetPriority(TIMER1_IRQn, 8);
	NVIC_EnableIRQ(TIMER1_IRQn);

	TIM_ResetCounter(LPC_TIM1);
	LPC_TIM1->TCR |= TIM_RESET;
	TIM_Cmd(LPC_TIM1, ENABLE);

	messageQueue = xQueueCreate(2, sizeof(IRRawMessage));
	xTaskCreate(ir_task, (signed portCHAR *)"ir", 512, NULL, 4, NULL);
}