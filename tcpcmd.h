#ifndef NIXIE_TCPCMD_H_
#define NIXIE_TCPCMD_H_

/**
 * Initializes the TCP command module.
 */
void tcpcmd_init(void);

#endif /* NIXIE_TCPCMD_H_ */