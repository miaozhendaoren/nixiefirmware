#include "iap.h"

#include <system_LPC17xx.h>

#define CMD_PREPARE_SECTORS				50
#define CMD_COPY_RAM_TO_FLASH			51
#define CMD_READ_DEVICE_SERIAL_NUMBER	58

//The location of the IAP ROM routine.
#define IAP_LOCATION 0x1FFF1FF1
typedef void (*IAP)(uint32_t *, uint32_t *);
IAP iap_entry = (IAP)IAP_LOCATION;

uint32_t iap_prepareSectors(uint32_t start, uint32_t end)
{
	uint32_t param[3] = { CMD_PREPARE_SECTORS, start, end };
	uint32_t ret;
	iap_entry(param, &ret);
	return ret;
}

uint32_t iap_copyRamToFlash(const uint8_t *dst, const uint8_t *src, uint32_t len)
{
	uint32_t param[5] = {
		CMD_COPY_RAM_TO_FLASH,
		(uint32_t)dst, (uint32_t)src, len,
		SystemCoreClock / 1000 };
	uint32_t ret;
	iap_entry(param, &ret);
	return ret;
}

void iap_readDeviceSerialNumber(uint32_t *buf)
{
	uint32_t param = CMD_READ_DEVICE_SERIAL_NUMBER;
	iap_entry(&param, buf);
}