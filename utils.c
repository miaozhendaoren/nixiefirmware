#include "utils.h"

#if BYTE_ORDER == LITTLE_ENDIAN

uint64_t hton64(uint64_t v)
{
	return
		((v & 0x00000000000000FF) << 56) |
		((v & 0x000000000000FF00) << 40) |
		((v & 0x0000000000FF0000) << 24) |
		((v & 0x00000000FF000000) <<  8) |
		((v & 0x000000FF00000000) >>  8) |
		((v & 0x0000FF0000000000) >> 24) |
		((v & 0x00FF000000000000) >> 40) |
		((v & 0xFF00000000000000) >> 56);

}

uint64_t ntoh64(uint64_t v)
{
	return hton64(v);
}

#endif