#include "network.h"

#include <FreeRTOS.h>
#include <task.h>

#include <string.h>

#include <lwip/ip_addr.h>
#include <lwip/tcpip.h>
#include <lwip/netifapi.h>
#include <lpc17_emac.h>

#include <lpc_phy.h>

struct netif lpcnet;
int tcpipIsReady;

static void network_tcpipReady(void *param);
static void network_updatePhyStatus(void *ctx);
static void network_installStatusCallback(void *ctx);
static void network_statusCallback(struct netif *netif);
static void network_task(void *param);

void network_tcpipReady(void *param)
{
	tcpipIsReady = 1;
}

void network_updatePhyStatus(void *ctx)
{
	lpc_phy_sts_sm(&lpcnet);
}

void network_installStatusCallback(void *ctx)
{
	netif_set_status_callback(&lpcnet, network_statusCallback);
}

void network_statusCallback(struct netif *netif)
{
	iprintf("Net status: addr = %d.%d.%d.%d, netmask = %d.%d.%d.%d, gw = %d.%d.%d.%d\n",
		ip4_addr1(&netif->ip_addr),
		ip4_addr2(&netif->ip_addr),
		ip4_addr3(&netif->ip_addr),
		ip4_addr4(&netif->ip_addr),
		ip4_addr1(&netif->netmask),
		ip4_addr2(&netif->netmask),
		ip4_addr3(&netif->netmask),
		ip4_addr4(&netif->netmask),
		ip4_addr1(&netif->gw),
		ip4_addr2(&netif->gw),
		ip4_addr3(&netif->gw),
		ip4_addr4(&netif->gw));
}

void network_task(void *param)
{
	tcpip_init(network_tcpipReady, NULL);
	while (!tcpipIsReady)
		taskYIELD();

	memset(&lpcnet, 0, sizeof(lpcnet));
	netifapi_netif_add(
		&lpcnet,
		IP_ADDR_ANY, IP_ADDR_ANY, IP_ADDR_ANY,
		NULL, lpc_enetif_init, tcpip_input);
	tcpip_callback(network_installStatusCallback, NULL);

	netifapi_netif_set_default(&lpcnet);

	/* Enable MAC interrupts */
	NVIC_SetPriority(ENET_IRQn, 9);
	NVIC_EnableIRQ(ENET_IRQn);

	netifapi_dhcp_start(&lpcnet);

	for (;;) {
		tcpip_callback(network_updatePhyStatus, NULL);
		vTaskDelay(configTICK_RATE_HZ);
	}
}

void network_init(void)
{
	xTaskCreate(network_task, (signed portCHAR *)"network", 256, NULL, 2, NULL);
}

ip_addr_t network_getAddr()
{
	return lpcnet.ip_addr;
}