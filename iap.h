#ifndef NIXIE_IAP_H_
#define NIXIE_IAP_H_

#include <stdint.h>

#define STATUS_CMD_SUCCESS 								0
#define STATUS_INVALID_COMMAND							1
#define STATUS_SRC_ADDR_ERROR							2
#define STATUS_DST_ADDR_ERROR							3
#define STATUS_SRC_ADDR_NOT_MAPPED						4
#define STATUS_DST_ADDR_NOT_MAPPED						5
#define STATUS_COUNT_ERROR								6
#define STATUS_INVALID_SECTOR							7
#define STATUS_SECTOR_NOT_BLANK							8
#define STATUS_SECTOR_NOT_PREPARED_FOR_WRITE_OPERATION	9
#define STATUS_COMPARE_ERROR							10
#define STATUS_BUSTY									11

/**
 * Prepares sectors for writing.
 *
 * @param start  The start sector.
 * @param end    The end sector.
 *
 * @return The return code as described in the LPC17xx user manual.
 */
uint32_t iap_prepareSectors(uint32_t start, uint32_t end);

/**
 * Copies RAM to flash. Sectors must be prepared before copying.
 *
 * @param dst  The destination flash address. Must be on a 256 byte boundary.
 * @param src  The source memory address. Must be on a word boundary.
 * @param len  The number of bytes to write. Must be 256, 512, 1024 or 4096.
 *
 * @return The return code as described in the LPC17xx user manual.
 */
uint32_t iap_copyRamToFlash(const uint8_t *dst, const uint8_t *src, uint32_t len);

/**
* Reads the device serial number.
*
* @param buf  The serial number will be written to this buffer. The buffer
*             must be larger than the serial number, 16 bytes (4 x 32 bit).
*/
void iap_readDeviceSerialNumber(uint32_t *buf);

#endif /* NIXIE_IAP_H_ */