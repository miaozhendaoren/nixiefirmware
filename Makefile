###########################
# Toolchain configuration
###########################
TOOLCHAIN_PREFIX = /Users/shadewind/SDK/yagarto-4.7.2/bin/
CC = $(TOOLCHAIN_PREFIX)arm-none-eabi-gcc
OBJCOPY = $(TOOLCHAIN_PREFIX)arm-none-eabi-objcopy
CFLAGS = \
		-ggdb -mthumb -mcpu=cortex-m3 -O0 -std=gnu99 -Wall \
		-ffunction-sections -fdata-sections \
		-nostartfiles \
		$(INCLUDES) \
		-DDEBUG

LDFLAGS=$(CFLAGS) -T link.ld -Wl,--gc-sections

#########
# Files
#########
SRCS = \
	main.c startup.c mdio.c serial.c syscall.c mem.c clock.c network.c \
	sntp.c utils.c ui.c tcpcmd.c iap.c nixie.c ir.c prop.c
OBJS = $(SRCS:.c=.o)

INCLUDES = -I.

############
# Libraries
############

# LPC Driver Library
LPC_BASE = lib/lpc/Drivers
SRCS += \
		$(LPC_BASE)/source/lpc17xx_gpio.c \
		$(LPC_BASE)/source/lpc17xx_pinsel.c \
		$(LPC_BASE)/source/lpc17xx_clkpwr.c \
		$(LPC_BASE)/source/lpc17xx_uart.c \
		$(LPC_BASE)/source/lpc17xx_ssp.c \
		$(LPC_BASE)/source/lpc17xx_pwm.c \
		$(LPC_BASE)/source/lpc17xx_timer.c \
		$(LPC_BASE)/source/lpc17xx_libcfg_default.c
INCLUDES += -I$(LPC_BASE)/include

# CMSIS
CMSIS_BASE = lib/lpc/Core
SRCS += $(CMSIS_BASE)/Device/NXP/LPC17xx/Source/system_LPC17xx.c
INCLUDES += \
		-I$(CMSIS_BASE)/CMSIS/Include \
		-I$(CMSIS_BASE)/Device/NXP/LPC17xx/Include

# FreeRTOS
FREERTOS_BASE = lib/FreeRTOS
FREERTOS_PORT_BASE = $(FREERTOS_BASE)/portable/GCC/ARM_CM3
SRCS += \
		$(FREERTOS_BASE)/tasks.c \
		$(FREERTOS_BASE)/queue.c \
		$(FREERTOS_BASE)/list.c \
		$(FREERTOS_PORT_BASE)/port.c
INCLUDES += \
		-I$(FREERTOS_BASE)/include \
		-I$(FREERTOS_PORT_BASE)

# lwIP
LWIP_BASE = lib/lwip/src
INCLUDES += \
	-I$(LWIP_BASE)/include \
	-I$(LWIP_BASE)/include/ipv4
SRCS += \
	$(LWIP_BASE)/core/init.c \
	$(LWIP_BASE)/core/timers.c \
	$(LWIP_BASE)/core/memp.c \
	$(LWIP_BASE)/core/dhcp.c \
	$(LWIP_BASE)/core/pbuf.c \
	$(LWIP_BASE)/core/mem.c \
	$(LWIP_BASE)/core/tcp.c \
	$(LWIP_BASE)/core/tcp_in.c \
	$(LWIP_BASE)/core/tcp_out.c \
	$(LWIP_BASE)/core/def.c \
	$(LWIP_BASE)/core/udp.c \
	$(LWIP_BASE)/core/dns.c \
	$(LWIP_BASE)/core/netif.c \
	$(LWIP_BASE)/core/stats.c \
	$(LWIP_BASE)/core/ipv4/ip.c \
	$(LWIP_BASE)/core/ipv4/ip_frag.c \
	$(LWIP_BASE)/core/ipv4/ip_addr.c \
	$(LWIP_BASE)/core/ipv4/inet_chksum.c \
	$(LWIP_BASE)/core/ipv4/icmp.c \
	$(LWIP_BASE)/api/tcpip.c \
	$(LWIP_BASE)/api/netifapi.c \
	$(LWIP_BASE)/api/netbuf.c \
	$(LWIP_BASE)/api/api_lib.c \
	$(LWIP_BASE)/api/api_msg.c \
	$(LWIP_BASE)/netif/etharp.c

# lwIP LPC port
LWIP_LPC_BASE = lpc_lwip
INCLUDES += \
	-I$(LWIP_LPC_BASE) \
	-I$(LWIP_LPC_BASE)/lpc_emac
SRCS += \
	$(LWIP_LPC_BASE)/lpc_emac/lpc17_emac.c \
	$(LWIP_LPC_BASE)/lpc_emac/lpc_board.c \
	$(LWIP_LPC_BASE)/lpc_emac/lpc_phy_ksz8051mnl.c \
	$(LWIP_LPC_BASE)/lpc_emac/lpc_arch.c \
	$(LWIP_LPC_BASE)/arch/sys_arch.c

##########
# OpenOCD
##########
OPENOCD_FLAGS = -f openocd/config.cfg

#########
# Rules
#########

nixie.bin: nixie.elf
	$(OBJCOPY) -O binary nixie.elf $@

nixie.elf: $(OBJS) link.ld
	$(CC) $(LDFLAGS) $(OBJS) -o $@

clean:
	rm -f $(OBJS)
	rm -f nixie.elf
	rm -f nixie.bin

upload: nixie.bin
	openocd $(OPENOCD_FLAGS) -c "set IMAGE_FILE nixie.bin" -f openocd/upload.cfg

openocd:
	openocd $(OPENOCD_FLAGS)

.PHONY: openocd