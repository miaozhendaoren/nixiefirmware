#include "udpcmd.h"

#include <FreeRTOS.h>
#include <task.h>

#include <lwip/api.h>
#include <lwip/netbuf.h>

#include <string.h>

void udpcmd_task(void *params)
{
	struct netconn *conn = netconn_new(NETCONN_UDP);
	if (conn == NULL) {
		//We can't do anything with a netconn :(
		vTaskDelete(NULL);
	}

	if (netconn_bind(conn, IP_ADDR_ANY, UDPCMD_PORT) != ERR_OK) {
		//...or without being able to bind.
		netconn_delete(conn);
		vTaskDelete(NULL);
	}

	for (;;) {
		struct netbuf *inbuf;
		if (netconn_recv(conn, &inbuf) != ERR_OK) {
			netconn_delete(conn);
			vTaskDelete(NULL);
		}

		int len = netbuf_copy_partial(inbuf, inputBuffer, UDPCMD_MAX_LENGTH, 0);
		ip_addr_t replyAddr = *netbuf_fromaddr(inbuf);
		int replyPort = netbuf_fromport(inbuf);
		netbuf_delete(inbuf);
		inputBuffer[len] = 0;

		int replyLength = udpcmd_handleCmd(inputBuffer, replyBuffer);
		if (replyLength > 0) {
			struct netbuf *replyNetbuf = netbuf_new();
			if (replyNetbuf == NULL)
				continue;
			
			if (netbuf_ref(replyNetbuf, replyBuffer, replyLength) == ERR_OK) {
				netconn_sendto(conn, replyNetbuf, &replyAddr, replyPort);
			}

			netbuf_delete(replyNetbuf);
		}
	}
}

void udpcmd_init(void)
{
	xTaskCreate(udpcmd_task, (signed portCHAR *)"udpcmd", 512, NULL, 2, NULL);
}