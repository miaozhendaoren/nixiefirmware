#include <sys/types.h>
#include <stdlib.h>
#include <malloc.h>

#include <FreeRTOS.h>
#include <task.h>

extern unsigned char _heap;
extern unsigned char _eheap;

void *pvPortMalloc(size_t xWantedSize)
{
	return malloc(xWantedSize);
}

void vPortFree(void *pv)
{
	free(pv);
}

void __malloc_lock (struct _reent *reent)
{
	vTaskSuspendAll();
}

void __malloc_unlock (struct _reent *reent)
{
	xTaskResumeAll();
}

caddr_t _sbrk(int incr)
{
	static unsigned char *heapEnd = &_heap;

	unsigned char *prevHeapEnd = heapEnd;
	if (heapEnd + incr > &_eheap) {
		for (;;);
	}

	heapEnd += incr;
	return (caddr_t) prevHeapEnd;
}