#ifndef NIXIE_CLOCK_H_
#define NIXIE_CLOCK_H_

#include <stdint.h>

/**
 * Initializes the clock system.
 */
void clock_init(void);

/**
 * Returns the current time.
 *
 * @return The current time in milliseconds Unix time.
 */
uint64_t clock_currentTime(void);

/**
 * Offsets the clock by the specified amount.
 *
 * @param offset  The offset in milliseconds.
 *
 * @return The new time.
 */
uint64_t clock_offset(int64_t offset);

/**
 * Clock interrupt handler.
 */
void clock_interrupt(void);

#endif /* NIXIE_CLOCK_H_ */