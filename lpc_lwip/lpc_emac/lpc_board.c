#include "lpc_board.h"

#include <arch/cc.h>

uint8_t mem_heap[16 * 1024];

void board_get_macaddr(u8_t *macaddr)
{
	macaddr[0] = LPC_EMAC_ADDR0;
	macaddr[1] = LPC_EMAC_ADDR1;
	macaddr[2] = LPC_EMAC_ADDR2;
	macaddr[3] = LPC_EMAC_ADDR3;
	macaddr[4] = LPC_EMAC_ADDR4;
	macaddr[5] = LPC_EMAC_ADDR5;
}

#ifdef LWIP_DEBUG

void assert_printf(char *msg, int line, char *file)
{
	iprintf("Assertion failed (%s:%d): %s\n", file, line, msg);
}

#endif /* LWIP_DEBUG */
