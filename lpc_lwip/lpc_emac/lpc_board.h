#ifndef __LPC_BOARD_H
#define __LPC_BOARD_H

#include "lwip/opt.h"

#define LPC_EMAC_ADDR0 0x00 /**< Hardware MAC address field 0 */
#define LPC_EMAC_ADDR1 0x60 /**< Hardware MAC address field 1 */
#define LPC_EMAC_ADDR2 0x37 /**< Hardware MAC address field 2 */
#define LPC_EMAC_ADDR3 0x12 /**< Hardware MAC address field 3 */
#define LPC_EMAC_ADDR4 0x34 /**< Hardware MAC address field 4 */
#define LPC_EMAC_ADDR5 0x56 /**< Hardware MAC address field 5 */

void board_get_macaddr(u8_t *macaddr);

#endif /* __LPC_BOARD_H */