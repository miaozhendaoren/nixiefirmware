#include "FreeRTOS.h"
#include "task.h"

#include "lwip/opt.h"
#include "lwip/mem.h"
#include "lwip/sys.h"

#include "lpc_arch.h"

/** 
 * Delay for the specified number of milliSeconds.
 *
 * @param ms  The number of milliseconds to delay.
 */
void msDelay(uint32_t ms)
{
	portTickType xDelayTime;

	xDelayTime = xTaskGetTickCount();
	vTaskDelayUntil( &xDelayTime, ms );
}