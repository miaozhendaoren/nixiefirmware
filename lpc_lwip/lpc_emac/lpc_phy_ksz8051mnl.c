#include "lpc_phy.h"

#include <lpc17xx_pinsel.h>
#include <lpc17xx_gpio.h>

#include "mdio.h"
#include "lpc_arch.h"

// KSZ8051MNL register addresses
#define REG_BASIC_CONTROL			0x00
#define REG_BASIC_STATUS			0x01
#define REG_PHY_ID1					0x02
#define REG_PHY_ID2					0x03
#define REG_AUTONEG_ADVERT			0x04
#define REG_AUTONEG_PARTNER_AB		0x05
#define REG_AUTONEG_EXPANSION		0x06
#define REG_AUTONEG_NEXT_PAGE		0x07
#define REG_PARTNER_NEXT_PAGE		0x08
#define REG_AFE_CONTROL1			0x11
#define REG_RXER_COUNTER			0x15
#define REG_OPMODE_STRAP_OVERRIDE	0x16
#define REG_OPMODE_STRAP_STATUS		0x17
#define REG_EXPANDED_CONTROL		0x18
#define REG_INTERRUPT				0x1B
#define REG_LINKMD					0x1D
#define REG_PHY_CONTROL1			0x1E
#define REG_PHY_CONTROL2			0x1F

#define OPMODE_NEGOTIATING			0x00
#define OPMODE_10BASE_T_HALF		0x01
#define OPMODE_100BASE_TX_HALF		0x02
#define OPMODE_10BASE_T_FULL		0x05
#define OPMODE_100BASE_TX_FULL		0x06

struct PHYStatusType {
	unsigned int speed100		: 1;
	unsigned int fullDuplex		: 1;
	unsigned int linkActive		: 1;

};

struct PHYStatusType phyStatus;

err_t lpc_phy_init(struct netif *netif, int rmii)
{
	PINSEL_CFG_Type cfg;
	cfg.OpenDrain = PINSEL_PINMODE_NORMAL;
	cfg.Pinmode = PINSEL_PINMODE_TRISTATE;

	// Set up strapping pins.
	cfg.Portnum = 1;
	cfg.Funcnum = 0;
	
	cfg.Pinnum = 10;
	PINSEL_ConfigPin(&cfg);
	FIO_SetDir(1, (1 << 10), 1);
	FIO_ClearValue(1, (1 << 10));

	cfg.Pinnum = 15;
	PINSEL_ConfigPin(&cfg);
	FIO_SetDir(1, (1 << 15), 1);
	FIO_SetValue(1, (1 << 15));

	cfg.Pinnum = 8;
	PINSEL_ConfigPin(&cfg);
	FIO_SetDir(1, (1 << 8), 1);
	FIO_ClearValue(1, (1 << 8));

	// Reset the PHY
	cfg.Portnum = 4;
	cfg.Pinnum = 29;
	cfg.Funcnum = 0;
	PINSEL_ConfigPin(&cfg);
	FIO_SetDir(4, (1 << 29), 1);
	FIO_ClearValue(4, (1 << 29));
	msDelay(100);
	FIO_SetValue(4, (1 << 29));
	msDelay(100);

	cfg.Portnum = 1;
	cfg.Funcnum = 1;

	cfg.Pinnum = 0;
	PINSEL_ConfigPin(&cfg);
	cfg.Pinnum = 1;
	PINSEL_ConfigPin(&cfg);
	cfg.Pinnum = 4;
	PINSEL_ConfigPin(&cfg);
	cfg.Pinnum = 9;
	PINSEL_ConfigPin(&cfg);
	cfg.Pinnum = 10;
	PINSEL_ConfigPin(&cfg);
	cfg.Pinnum = 8;
	PINSEL_ConfigPin(&cfg);
	cfg.Pinnum = 14;
	PINSEL_ConfigPin(&cfg);
	cfg.Pinnum = 15;
	PINSEL_ConfigPin(&cfg);

	mdio_init();
	return ERR_OK;
}

s32_t lpc_phy_sts_sm(struct netif *netif)
{
	struct PHYStatusType newStatus = { 0, 0, 0 };

	newStatus.linkActive = ((mdio_read(REG_BASIC_STATUS) & 0x04) != 0);
	if (newStatus.linkActive) {
		uint16_t phyControl1 = mdio_read(REG_PHY_CONTROL1);
		uint8_t opmode = phyControl1 & 0x07;
		switch (opmode) {
			case OPMODE_10BASE_T_HALF:
				newStatus.speed100 = 0;
				newStatus.fullDuplex = 0;
				break;

			case OPMODE_100BASE_TX_HALF:
				newStatus.speed100 = 1;
				newStatus.fullDuplex = 0;
				break;

			case OPMODE_10BASE_T_FULL:
				newStatus.speed100 = 0;
				newStatus.fullDuplex = 1;
				break;

			case OPMODE_100BASE_TX_FULL:
				newStatus.speed100 = 1;
				newStatus.fullDuplex = 1;
				break;
		}
	}

	int changed = 0;
	
	if (newStatus.speed100 != phyStatus.speed100) {
		changed = 1;
		lpc_emac_set_speed(newStatus.speed100);
	}

	if (newStatus.fullDuplex != phyStatus.fullDuplex) {
		changed = 1;
		lpc_emac_set_duplex(newStatus.fullDuplex);
	}

	if (newStatus.linkActive != phyStatus.linkActive) {
		changed = 1;
		if(newStatus.linkActive)
			netif_set_link_up(netif);
		else
			netif_set_link_up(netif);
	}

	phyStatus = newStatus;
	return changed;
}
