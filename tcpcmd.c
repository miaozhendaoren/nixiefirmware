#include "tcpcmd.h"

#include <FreeRTOS.h>
#include <task.h>

#include <lwip/api.h>
#include <lwip/netbuf.h>

#include <string.h>

#include "prop.h"

#define TCPCMD_PORT 19191
#define TCPCMD_BUFFER_SIZE 512

static char *splitToken(char *str);
static void tcpcmd_handleCmd(struct netconn *conn, char *cmd);
static void tcpcmd_handleCmdPing(struct netconn *conn, char *params);
static void tcpcmd_handleCmdGetprop(struct netconn *conn, char *params);
static void tcpcmd_handleCmdSetprop(struct netconn *conn, char *params);
static void tcpcmd_handleCmdSave(struct netconn *conn, char *params);
static void tcpcmd_handleCmdQuit(struct netconn *conn, char *params);
static void tcpcmd_task(void *params);
static void tcpcmd_serverLoop(struct netconn *conn);
static int tcpcmd_handleLines(struct netconn *conn);

typedef void (*HandlerFunc)(struct netconn *, char *);

typedef struct {
	const char *name;
	HandlerFunc handler;
} CommandEntry;

static const CommandEntry handlers[] = {
	{ "ping", tcpcmd_handleCmdPing },
	{ "getprop", tcpcmd_handleCmdGetprop },
	{ "setprop", tcpcmd_handleCmdSetprop },
	{ "save", tcpcmd_handleCmdSave },
	{ "quit", tcpcmd_handleCmdQuit },
	{ "exit", tcpcmd_handleCmdQuit },
	{ NULL, NULL }
};

static char inputBuffer[TCPCMD_BUFFER_SIZE];
static size_t inBufferPos;

char *splitToken(char *str)
{
	char *split = str;
	while((*split != ' ') && (*split != 0))
		split++;
	*split = 0;
	split++;

	return split;
}

void tcpcmd_handleCmd(struct netconn *conn, char *cmd)
{
	char *split = splitToken(cmd);
	for (int i = 0; handlers[i].handler != NULL; i++) {
		if (strcmp(handlers[i].name, cmd) == 0)
			return handlers[i].handler(conn, split);
	}
}

void tcpcmd_handleCmdPing(struct netconn *conn, char *params)
{
	netconn_write(conn, "pong\r\n", 6, 0);
}

void tcpcmd_handleCmdGetprop(struct netconn *conn, char *params)
{
	char value[PROP_VALUE_MAX_LEN + 1];
	int len = prop_get(params, value, sizeof(value));
	netconn_write(conn, value, len, NETCONN_COPY | NETCONN_MORE);
	netconn_write(conn, "\r\n", 2, 0);
}

void tcpcmd_handleCmdSetprop(struct netconn *conn, char *params)
{
	char *split = splitToken(params);
	prop_set(params, split);
}

void tcpcmd_handleCmdSave(struct netconn *conn, char *params)
{
	prop_savePersistent();
}

void tcpcmd_handleCmdQuit(struct netconn *conn, char *params)
{
	netconn_shutdown(conn, 0, 1);
}

void tcpcmd_task(void *params)
{
	struct netconn *conn = netconn_new(NETCONN_TCP);
	if (conn == NULL) {
		//We can't do anything with a netconn :(
		vTaskDelete(NULL);
	}

	if (netconn_bind(conn, IP_ADDR_ANY, TCPCMD_PORT) != ERR_OK) {
		//...or without being able to bind.
		netconn_delete(conn);
		vTaskDelete(NULL);
	}

	if (netconn_listen(conn) != ERR_OK) {
		netconn_delete(conn);
		vTaskDelete(NULL);
	}

	for (;;) {
		struct netconn *acceptedConn;
		if (netconn_accept(conn, &acceptedConn) == ERR_OK) {
			tcpcmd_serverLoop(acceptedConn);
			netconn_delete(acceptedConn);
		}
	}
}

void tcpcmd_serverLoop(struct netconn *conn)
{
	inBufferPos = 0;
	while (conn->state != NETCONN_CLOSE) {
		struct netbuf *inbuf;
		if (netconn_recv(conn, &inbuf) != ERR_OK)
			break;

		//If line is too long, just discard it.
		if (inBufferPos == TCPCMD_BUFFER_SIZE)
			inBufferPos = 0;

		uint16_t netbufRead = 0;
		while (netbufRead < netbuf_len(inbuf)) {
			uint16_t copied = netbuf_copy_partial(
				inbuf,
				inputBuffer + inBufferPos,
				TCPCMD_BUFFER_SIZE - inBufferPos,
				netbufRead);
			inBufferPos += copied;
			netbufRead += copied;

			while (tcpcmd_handleLines(conn));
		}

		netbuf_delete(inbuf);
	}
}

int tcpcmd_handleLines(struct netconn *conn)
{
	int next = -1;
	for (int i = 0; i < inBufferPos; i++) {
		if ((inputBuffer[i] == '\r') || (inputBuffer[i] == '\n')) {
			char terminator = inputBuffer[i];
			inputBuffer[i] = 0;
			next = i + 1;
			if ((next < inBufferPos) && (terminator == '\r') && (inputBuffer[next] == '\n'))
				next++;
			break;
		}
	}

	if (next != -1) {
		tcpcmd_handleCmd(conn, inputBuffer);
		//Compact buffer
		memmove(inputBuffer, inputBuffer + next, inBufferPos - next);
		inBufferPos -= next;

		return 1;
	}

	return 0;
}

void tcpcmd_init(void)
{
	xTaskCreate(tcpcmd_task, (signed portCHAR *)"tcpcmd", 512, NULL, 2, NULL);
}