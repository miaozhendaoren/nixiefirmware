#include "clock.h"

#include <lpc17xx_clkpwr.h>
#include <lpc17xx_rit.h>

//Time defaults to 1 Jan, 2012
static uint64_t currentTime = 1325376000000;

void clock_init(void)
{
	CLKPWR_ConfigPPWR(CLKPWR_PCONP_PCRIT, ENABLE);
	CLKPWR_SetPCLKDiv(CLKPWR_PCLKSEL_RIT, CLKPWR_PCLKSEL_CCLK_DIV_4);
	uint32_t pclk = CLKPWR_GetPCLK(CLKPWR_PCLKSEL_RIT);

	LPC_RIT->RIMASK = 0;
	LPC_RIT->RICOMPVAL = pclk / 100;
	LPC_RIT->RICTRL = RIT_CTRL_ENCLR | RIT_CTRL_INTEN | RIT_CTRL_TEN;

	NVIC_SetPriority(RIT_IRQn, 7);
	NVIC_EnableIRQ(RIT_IRQn);	
}

uint64_t clock_currentTime(void)
{
	NVIC_DisableIRQ(RIT_IRQn);
	uint64_t t = currentTime;
	NVIC_EnableIRQ(RIT_IRQn);
	return t;
}

uint64_t clock_offset(int64_t offset)
{
	NVIC_DisableIRQ(RIT_IRQn);
	currentTime += offset;
	uint64_t t = currentTime;
	NVIC_EnableIRQ(RIT_IRQn);
	return t;
}

void clock_interrupt(void)
{
	currentTime += 10;
	LPC_RIT->RICTRL |= RIT_CTRL_INTEN;
}