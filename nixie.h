#ifndef NIXIE_NIXIE_H_
#define NIXIE_NIXIE_H_

#include <stdint.h>

/**
 * Initializes the nixie module.
 */
 void nixie_init(void);
 
/**
 * Sets the nixie display to display the specified digits. To display
 * the left and right point, use ',' and '.' respectively. To display
 * a space, simply use the space character.
 *
 * @param digits  An array of length 8 containing the digits to display.
 */
void nixie_setDisplay(char *digits);

/**
 * Fades between two digit strings.
 *
 * @param from  The string to fade from.
 * @param to    The string to fade to.
 * @param time  The lenght of the fade. 
 */
void nixie_fade(char *from, char *to, float time);

/**
 * Sets the brightness.
 *
 * @param value  The brightness value from 0 (off) up to 255 (max).
 */
void nixie_setBrightness(uint8_t value);

/**
 * Sets the backlight brightness.
 *
 * @param value  The brightness value from 0 (off) up to 255 (max).
 */
void nixie_setBacklight(uint8_t value);

#endif /* NIXIE_NIXIE_H_ */