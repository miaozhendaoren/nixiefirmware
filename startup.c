#include <stdint.h>

#include "clock.h"
#include "ir.h"

//Linker symbols
extern unsigned long _etext;
extern unsigned long _datalma;
extern unsigned long _data;
extern unsigned long _edata;
extern unsigned long _bss;
extern unsigned long _ebss;
extern unsigned long _ahb_bss0;
extern unsigned long _eahb_bss0;
extern unsigned long _ahb_bss1;
extern unsigned long _eahb_bss1;

//This is not a function but the compiler wants void funcs for the vector table
void _esram();

//CMSIS init function
extern void SystemInit();

//Main entry point
extern int main(); 

//Basic interrupts
static void resetInterrupt();
static void nonMaskableInterrupt();
static void faultInterrupt() __attribute__((naked));
static void defaultInterrupt();

//OS interrupts
extern void xPortSysTickHandler();
extern void xPortPendSVHandler();
extern void vPortSVCHandler();

//Other interrupts
extern void ENET_IRQHandler();

// Interrupt vector
__attribute__ ((section(".isr_vector")))
void (* const isrVector[])() = {
	&_esram,				/* Top of Stack                 */
	resetInterrupt,			/* Reset Handler                */
	nonMaskableInterrupt,	/* NMI Handler                  */
	faultInterrupt,			/* Hard Fault Handler           */
	defaultInterrupt,		/* MPU Fault Handler            */
	defaultInterrupt,		/* Bus Fault Handler            */
	defaultInterrupt,		/* Usage Fault Handler          */
	0,						/* Reserved                     */
	0,						/* Reserved                     */
	0,						/* Reserved                     */
	0,						/* Reserved                     */
	vPortSVCHandler,		/* SVCall Handler               */
	defaultInterrupt,		/* Debug Monitor Handler        */
	0,						/* Reserved                     */
	xPortPendSVHandler,		/* PendSV Handler               */
	xPortSysTickHandler,	/* SysTick Handler              */

    /* External Interrupts */
	defaultInterrupt,		/* 16: Watchdog Timer               */
	defaultInterrupt,		/* 17: Timer0                       */
	ir_interrupt,			/* 18: Timer1                       */
	defaultInterrupt,		/* 19: Timer2                       */
	defaultInterrupt,		/* 20: Timer3                       */
	defaultInterrupt,		/* 21: UART0                        */
	defaultInterrupt,		/* 22: UART1                        */
	defaultInterrupt,		/* 23: UART2                        */
	defaultInterrupt,		/* 24: UART3                        */
	defaultInterrupt,		/* 25: PWM1                         */
	defaultInterrupt,		/* 26: I2C0                         */
	defaultInterrupt,		/* 27: I2C1                         */
	defaultInterrupt,		/* 28: I2C2                         */
	defaultInterrupt,		/* 29: SPI                          */
	defaultInterrupt,		/* 30: SSP0                         */
	defaultInterrupt,		/* 31: SSP1                         */
	defaultInterrupt,		/* 32: PLL0 Lock (Main PLL)         */
	defaultInterrupt,		/* 33: Real Time Clock              */
	defaultInterrupt,		/* 34: External Interrupt 0         */
	defaultInterrupt,		/* 35: External Interrupt 1         */
	defaultInterrupt,		/* 36: External Interrupt 2         */
	defaultInterrupt,		/* 37: External Interrupt 3         */
	defaultInterrupt,		/* 38: A/D Converter                */
	defaultInterrupt,		/* 39: Brown-Out Detect             */
	defaultInterrupt,		/* 40: USB                          */
	defaultInterrupt,		/* 41: CAN                          */
	defaultInterrupt,		/* 42: General Purpose DMA          */
	defaultInterrupt,		/* 43: I2S                          */
	ENET_IRQHandler,		/* 44: Ethernet                     */
	clock_interrupt,		/* 45: Repetitive Interrupt Timer   */
	defaultInterrupt,		/* 46: Motor Control PWM            */
	defaultInterrupt,		/* 47: Quadrature Encoder Interface */
	defaultInterrupt,		/* 48: PLL1 Lock (USB PLL)          */
	defaultInterrupt,		/* 49: USB Activity 				*/
	defaultInterrupt		/* 50: CAN Activity					*/
};

void resetInterrupt(void)
{
	unsigned long *src, *dest;

	//Load data from FLASH to SRAM
	src = &_datalma;
	for (dest = &_data; dest < &_edata; )
		*dest++ = *src++;
	
	//Zero BSS
	for (dest = &_bss; dest < &_ebss; dest++)
		*dest = 0;
	//Zero AHB SRAM block 0 BSS section
	for (dest = &_ahb_bss0; dest < &_eahb_bss0; dest++)
		*dest = 0;
	
	//Zero AHB SRAM block 1 BSS section
	for (dest = &_ahb_bss1; dest < &_eahb_bss1; dest++)
		*dest = 0;

	SystemInit();
	main();
}

static void nonMaskableInterrupt(void)
{
	for(;;);
}

void getRegistersFromStack(uint32_t *pulFaultStackAddress)
{
	/* These are volatile to try and prevent the compiler/linker optimising them
	away as the variables never actually get used.  If the debugger won't show the
	values of the variables, make them global my moving their declaration outside
	of this function. */
	volatile uint32_t r0;
	volatile uint32_t r1;
	volatile uint32_t r2;
	volatile uint32_t r3;
	volatile uint32_t r12;
	volatile uint32_t lr; /* Link register. */
	volatile uint32_t pc; /* Program counter. */
	volatile uint32_t psr;/* Program status register. */
	volatile unsigned long _CFSR;
	volatile unsigned long _HFSR;
	volatile unsigned long _DFSR;
	volatile unsigned long _AFSR;
	volatile unsigned long _BFAR;
	volatile unsigned long _MMAR;

	r0 = pulFaultStackAddress[0];
	r1 = pulFaultStackAddress[1];
	r2 = pulFaultStackAddress[2];
	r3 = pulFaultStackAddress[3];

	r12 = pulFaultStackAddress[4];
	lr = pulFaultStackAddress[5];
	pc = pulFaultStackAddress[6];
	psr = pulFaultStackAddress[7];

	// Configurable Fault Status Register
	// Consists of MMSR, BFSR and UFSR
	_CFSR = (*((volatile unsigned long *)(0xE000ED28)));

	// Hard Fault Status Register
	_HFSR = (*((volatile unsigned long *)(0xE000ED2C)));

	// Debug Fault Status Register
	_DFSR = (*((volatile unsigned long *)(0xE000ED30)));

	// Auxiliary Fault Status Register
	_AFSR = (*((volatile unsigned long *)(0xE000ED3C)));

	// Read the Fault Address Registers. These may not contain valid values.
	// Check BFARVALID/MMARVALID to see if they are valid values
	// MemManage Fault Address Register
	_MMAR = (*((volatile unsigned long *)(0xE000ED34)));
	// Bus Fault Address Register
	_BFAR = (*((volatile unsigned long *)(0xE000ED38)));

	/* When the following line is hit, the variables contain the register values. */
	for(;;);
}

static void faultInterrupt(void)
{
	__asm volatile
	(
		" tst lr, #4                                                \n"
		" ite eq                                                    \n"
		" mrseq r0, msp                                             \n"
		" mrsne r0, psp                                             \n"
		" ldr r1, [r0, #24]                                         \n"
		" ldr r2, handler2_address_const                            \n"
		" bx r2                                                     \n"
		" handler2_address_const: .word getRegistersFromStack	    \n"
	);
}

static void defaultInterrupt(void)
{
	for(;;);
}
